package org.southasia.ghru.vo.request

import android.os.Parcel
import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.southasia.ghru.BR
import org.southasia.ghru.vo.Meta
import java.io.Serializable


data class HeightMeasurementMetaResonce(
    @Expose @SerializedName("error") val meta: Boolean?,
    @Expose @SerializedName("data") val data: HeightWeightMeasurementMetaResonceData?
) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readParcelable(HeightWeightMeasurementMetaResonceData::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(meta)
        parcel.writeParcelable(data, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HeightMeasurementMetaResonce> {
        override fun createFromParcel(parcel: Parcel): HeightMeasurementMetaResonce {
            return HeightMeasurementMetaResonce(parcel)
        }

        override fun newArray(size: Int): Array<HeightMeasurementMetaResonce?> {
            return arrayOfNulls(size)
        }
    }


}

data class HeightWeightMeasurementMetaResonceData(
    @Expose @SerializedName("error") val meta: Boolean?,
    @Expose @SerializedName("station") val station: HeightWeightMeasurementMetaResonceStation?
) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readParcelable(HeightWeightMeasurementMetaResonceStation::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(meta)
        parcel.writeParcelable(station, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HeightWeightMeasurementMetaResonceData> {
        override fun createFromParcel(parcel: Parcel): HeightWeightMeasurementMetaResonceData {
            return HeightWeightMeasurementMetaResonceData(parcel)
        }

        override fun newArray(size: Int): Array<HeightWeightMeasurementMetaResonceData?> {
            return arrayOfNulls(size)
        }
    }

}

data class HeightWeightMeasurementMetaResonceStation(
    @Expose @SerializedName("error") val meta: Boolean?,
    @Expose @SerializedName("data") val data: HeightWeightMeasurementMeta?
) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readParcelable(HeightWeightMeasurementMeta::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(meta)
        parcel.writeParcelable(data, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HeightWeightMeasurementMetaResonceStation> {
        override fun createFromParcel(parcel: Parcel): HeightWeightMeasurementMetaResonceStation {
            return HeightWeightMeasurementMetaResonceStation(parcel)
        }

        override fun newArray(size: Int): Array<HeightWeightMeasurementMetaResonceStation?> {
            return arrayOfNulls(size)
        }
    }

}
@Entity(tableName = "height_weight_measurement_meta")
data class HeightWeightMeasurementMeta(
    @Embedded(prefix = "meta") @Expose @SerializedName("meta") val meta: Meta?,
    @Embedded(prefix = "body")@Expose @SerializedName("body") val body: HeightWeightMeasurement?
) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Meta::class.java.classLoader),
        parcel.readParcelable(HeightWeightMeasurement::class.java.classLoader)
    ) {
        id = parcel.readLong()
        timestamp = parcel.readLong()
        syncPending = parcel.readByte() != 0.toByte()
        screeningId = parcel.readString()
    }

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
    @ColumnInfo(name = "timestamp")
    var timestamp: Long = System.currentTimeMillis()

    @ColumnInfo(name = "sync_pending")
    var syncPending: Boolean = false

    @ColumnInfo(name = "screening_id")
    lateinit var screeningId: String

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(meta, flags)
        parcel.writeParcelable(body, flags)
        parcel.writeLong(id)
        parcel.writeLong(timestamp)
        parcel.writeByte(if (syncPending) 1 else 0)
        parcel.writeString(screeningId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HeightWeightMeasurementMeta> {
        override fun createFromParcel(parcel: Parcel): HeightWeightMeasurementMeta {
            return HeightWeightMeasurementMeta(parcel)
        }

        override fun newArray(size: Int): Array<HeightWeightMeasurementMeta?> {
            return arrayOfNulls(size)
        }
    }

}

data class HeightWeightMeasurement(
    @Expose @SerializedName("height") val height: HeightWeightMeasurementData?,
    @Expose @SerializedName("weight") val weight: HeightWeightMeasurementData?
) : Serializable, Parcelable {

    @Ignore
    @Expose
    @SerializedName("contraindications")
    var contraindications: List<Map<String, String>>? = null

    constructor(parcel: Parcel) : this(
        parcel.readParcelable(HeightWeightMeasurementData::class.java.classLoader),
        parcel.readParcelable(HeightWeightMeasurementData::class.java.classLoader)
    ) {
        readContraindications(parcel)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(height, flags)
        parcel.writeParcelable(weight, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HeightWeightMeasurement> {
        override fun createFromParcel(parcel: Parcel): HeightWeightMeasurement {
            return HeightWeightMeasurement(parcel)
        }

        override fun newArray(size: Int): Array<HeightWeightMeasurement?> {
            return arrayOfNulls(size)
        }

        private fun readContraindications(parcel: Parcel): List<Map<String, String>> {
            val list = mutableListOf<Map<String, String>>()
            parcel.readList(list as List<*>, Map::class.java.classLoader)

            return list
        }
    }

}


data class HeightWeightMeasurementData(
    @Expose @SerializedName("device_id") var deviceId: String?,
    @Expose @SerializedName("comment") var comment: String?,
    @Embedded(prefix = "data") @Expose @SerializedName("data") var data: HeightWeightValueData?,
    @Embedded(prefix = "skip")  @Expose @SerializedName("skip") var skip: CancelRequest?
) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(HeightWeightValueData::class.java.classLoader),
        parcel.readParcelable(CancelRequest::class.java.classLoader)
    ) {
    }

    constructor() : this(null, null, null, null)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(deviceId)
        parcel.writeString(comment)
        parcel.writeParcelable(data, flags)
        parcel.writeParcelable(skip, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HeightWeightMeasurementData> {
        override fun createFromParcel(parcel: Parcel): HeightWeightMeasurementData {
            return HeightWeightMeasurementData(parcel)
        }

        override fun newArray(size: Int): Array<HeightWeightMeasurementData?> {
            return arrayOfNulls(size)
        }
    }
}

data class HeightWeightValueData(
    @Embedded(prefix = "height") @Expose @SerializedName("height") val height: HeightWeightValueDto?,
    @Embedded(prefix = "fat_composition") @Expose @SerializedName("fat_composition") val fatComposition: HeightWeightValueDto?
) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(HeightWeightValueDto::class.java.classLoader),
        parcel.readParcelable(HeightWeightValueDto::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(height, flags)
        parcel.writeParcelable(fatComposition, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HeightWeightValueData> {
        override fun createFromParcel(parcel: Parcel): HeightWeightValueData {
            return HeightWeightValueData(parcel)
        }

        override fun newArray(size: Int): Array<HeightWeightValueData?> {
            return arrayOfNulls(size)
        }
    }

}

data class HeightWeightValueDto(
    @Expose @SerializedName("unit") val unit: String?,
    @Expose @SerializedName("value") val value: Double?

) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(unit)
        parcel.writeValue(value)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HeightWeightValueDto> {
        override fun createFromParcel(parcel: Parcel): HeightWeightValueDto {
            return HeightWeightValueDto(parcel)
        }

        override fun newArray(size: Int): Array<HeightWeightValueDto?> {
            return arrayOfNulls(size)
        }
    }
}

class HeightWeightValue : BaseObservable(), Serializable {

    companion object {
        fun build(): HeightWeightValue {
            val bodyMeasurementValue = HeightWeightValue()
            bodyMeasurementValue.unit = String()
            bodyMeasurementValue.value = String()
            bodyMeasurementValue.comment = String()
            bodyMeasurementValue.deviceId = String()
            return bodyMeasurementValue
        }
    }


    var value: String = String()
        set(value) {
            field = value
            notifyPropertyChanged(BR.value)
        }
        @Bindable get() = field


    var unit: String = String()
        set(value) {
            field = value
            notifyPropertyChanged(BR.unit)
        }
        @Bindable get() = field


    var comment: String = String()
        set(value) {
            field = value
            notifyPropertyChanged(BR.comment)
        }
        @Bindable get() = field


    var deviceId: String = String()
        set(value) {
            field = value
        }
        @Bindable get() = field


}