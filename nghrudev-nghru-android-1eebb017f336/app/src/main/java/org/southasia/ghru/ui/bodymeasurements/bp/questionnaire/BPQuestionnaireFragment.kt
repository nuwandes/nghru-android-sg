package org.southasia.ghru.ui.bodymeasurements.bp.questionnaire

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController

import org.southasia.ghru.R
import org.southasia.ghru.binding.FragmentDataBindingComponent
import org.southasia.ghru.databinding.BPQuestionnaireFragmentBinding
import org.southasia.ghru.ui.ecg.questions.ECGSkipFragment
import org.southasia.ghru.ui.ecg.questions.TYPE_BP
import org.southasia.ghru.util.autoCleared
import org.southasia.ghru.util.singleClick
import org.southasia.ghru.vo.request.ParticipantRequest
import javax.inject.Inject

class BPQuestionnaireFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var binding by autoCleared<BPQuestionnaireFragmentBinding>()
    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)
    @Inject
    lateinit var viewModel: BPQuestionnaireViewModel

    private var participantRequest: ParticipantRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            participantRequest = arguments?.getParcelable<ParticipantRequest>("ParticipantRequest")!!
        } catch (e: KotlinNullPointerException) {
            //Crashlytics.logException(e)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<BPQuestionnaireFragmentBinding>(
            inflater,
            R.layout.b_p_questionnaire_fragment,
            container,
            false
        )
        binding = dataBinding

        setHasOptionsMenu(true)
        val appCompatActivity = requireActivity() as AppCompatActivity
        appCompatActivity.setSupportActionBar(binding.detailToolbar)
        appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = activity?.run {
            ViewModelProviders.of(this).get(BPQuestionnaireViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.participant = participantRequest

        binding.setLifecycleOwner(this)

        binding.nextButton.singleClick {
            if (validateNextButton()) {
                val bundle = Bundle()
                bundle.putParcelable("ParticipantRequest", participantRequest)
                navController().navigate(R.id.action_bPQuestionnaireFragment_to_pPManualOneFragment, bundle)
            }
        }

        binding.radioGroupArteriovenous.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.noArteriovenous) {
                binding.radioGroupArteriovenousValue = false
                viewModel.setHaveArteriovenous(false)

            } else {
                binding.radioGroupArteriovenousValue = false
                viewModel.setHaveArteriovenous(true)

            }
            binding.executePendingBindings()
        }
        binding.radioGroupSurgery.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.noSurgery) {
                binding.radioGroupSurgeryValue = false
                viewModel.setHadSurgery(false)

            } else {
                binding.radioGroupSurgeryValue = false
                viewModel.setHadSurgery(true)

            }
            binding.executePendingBindings()
        }
        binding.radioGroupLymph.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.noLymph) {
                binding.radioGroupLymphValue = false
                viewModel.setLymphRemoved(false)

            } else {
                binding.radioGroupLymphValue = false
                viewModel.setLymphRemoved(true)

            }
            binding.executePendingBindings()
        }
        binding.radioGroupTrauma.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.noTrauma) {
                binding.radioGroupTraumaValue = false
                viewModel.setHaveTrauma(false)

            } else {
                binding.radioGroupTraumaValue = false
                viewModel.setHaveTrauma(true)

            }
            binding.executePendingBindings()
        }
    }

    private fun validateNextButton(): Boolean {
        if(viewModel.haveArteriovenous.value == null) {
            binding.radioGroupArteriovenousValue = true
            binding.executePendingBindings()
            return false
        }
        else if(viewModel.hadSurgery.value == null) {
            binding.radioGroupSurgeryValue = true
            binding.executePendingBindings()
            return false
        }
        else if(viewModel.lymphRemoved.value == null) {
            binding.radioGroupLymphValue = true
            binding.executePendingBindings()
            return false
        }
        else if(viewModel.haveTrauma.value == null) {
            binding.radioGroupTraumaValue = true
            binding.executePendingBindings()
            return false
        }

        if(viewModel.haveArteriovenous.value ==  true || viewModel.hadSurgery.value ==  true ||
            viewModel.lymphRemoved.value ==  true || viewModel.haveTrauma.value ==  true) {
            val skipDialogFragment = ECGSkipFragment()
            skipDialogFragment.arguments = bundleOf("participant" to participantRequest,
                "contraindications" to getContraindications(),
                "type" to TYPE_BP)
            skipDialogFragment.show(fragmentManager!!)

            return false
        }

        return true
    }

    private fun getContraindications(): MutableList<Map<String, String>> {
        var contraindications = mutableListOf<Map<String, String>>()

        val haveArteriovenous = viewModel.haveArteriovenous.value
        val hadSurgery = viewModel.hadSurgery.value
        val lymphRemoved = viewModel.lymphRemoved.value
        val haveTrauma = viewModel.haveTrauma.value

        if (haveArteriovenous!!) {
            var arteriovenousMap = mutableMapOf<String, String>()
            arteriovenousMap["question"] = "Have Arteriovenous Fistula?"
            arteriovenousMap["answer"] = if (haveArteriovenous!!) "yes" else "no"

            contraindications.add(arteriovenousMap)
        }

        if (hadSurgery!!) {
            var surgeryMap = mutableMapOf<String, String>()
            surgeryMap["question"] = "Breast Surgery?"
            surgeryMap["answer"] = if (hadSurgery!!) "yes" else "no"

            contraindications.add(surgeryMap)
        }

        if (lymphRemoved!!) {
            var lymphRemovedMap = mutableMapOf<String, String>()
            lymphRemovedMap["question"] = "Lymph Node removed from armpit?"
            lymphRemovedMap["answer"] = if (lymphRemoved!!) "yes" else "no"

            contraindications.add(lymphRemovedMap)
        }

        if (haveTrauma!!) {
            var traumaMap = mutableMapOf<String, String>()
            traumaMap["question"] = "Trauma --> Arm Swelling?"
            traumaMap["answer"] = if (haveTrauma!!) "yes" else "no"

            contraindications.add(traumaMap)
        }

        return contraindications
    }

    /**
     * Created to be able to override in tests
     */
    fun navController() = findNavController()

}
