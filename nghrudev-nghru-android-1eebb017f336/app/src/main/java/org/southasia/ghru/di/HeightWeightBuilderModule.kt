package org.southasia.ghru.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import org.southasia.ghru.ui.heightweight.HeightWeightHomeFragment
import org.southasia.ghru.ui.heightweight.completed.CompletedDialogFragment
import org.southasia.ghru.ui.heightweight.contraindication.HeightWeightQuestionnaireFragment
import org.southasia.ghru.ui.heightweight.contraindication.HeightWeightSkipFragment
import org.southasia.ghru.ui.heightweight.errordialog.ErrorDialogFragment
import org.southasia.ghru.ui.heightweight.height.HeightFragment
import org.southasia.ghru.ui.heightweight.height.reason.ReasonDialogFragment
import org.southasia.ghru.ui.heightweight.manualentry.ManualEntryHeightWeightFragment
import org.southasia.ghru.ui.heightweight.scanbarcode.ScanBarcodeFragment
import org.southasia.ghru.ui.heightweight.weight.WeightFragment
import org.southasia.ghru.ui.stationcheck.StationCheckDialogFragment

@Suppress("unused")
@Module
abstract class HeightWeightBuilderModule {

    @ContributesAndroidInjector
    abstract fun HeightWeightHomeFragment(): HeightWeightHomeFragment

    @ContributesAndroidInjector
    abstract fun ScanBarcodeFragment(): ScanBarcodeFragment

    @ContributesAndroidInjector
    abstract fun ManualEntryFragment(): ManualEntryHeightWeightFragment

    @ContributesAndroidInjector
    abstract fun HeightFragment(): HeightFragment

    @ContributesAndroidInjector
    abstract fun HeightWeightQuestionaryFragment(): HeightWeightQuestionnaireFragment

    @ContributesAndroidInjector
    abstract fun HeightWeightQuestionarySkipFragment(): HeightWeightSkipFragment

    @ContributesAndroidInjector
    abstract fun ReasonDialogFragment(): ReasonDialogFragment

    @ContributesAndroidInjector
    abstract fun StationCheckDialogFragment(): StationCheckDialogFragment

    @ContributesAndroidInjector
    abstract fun CompletedDialogFragment(): CompletedDialogFragment

    @ContributesAndroidInjector
    abstract fun ErrorDialogFragment(): ErrorDialogFragment

    @ContributesAndroidInjector
    abstract fun WeightFragment(): WeightFragment
}