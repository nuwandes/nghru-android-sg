package org.southasia.ghru.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import org.southasia.ghru.ui.heightweight.completed.CompletedDialogFragment
import org.southasia.ghru.ui.hipwaist.contraindication.HipWaistQuestionnaireFragment
import org.southasia.ghru.ui.hipwaist.contraindication.HipWaistSkipFragment
import org.southasia.ghru.ui.heightweight.errordialog.ErrorDialogFragment
import org.southasia.ghru.ui.hipwaist.reason.ReasonDialogFragment
import org.southasia.ghru.ui.hipwaist.manualentry.ManualEntryHipWaistFragment
import org.southasia.ghru.ui.hipwaist.ScanBarcodeFragment
import org.southasia.ghru.ui.hipwaist.hipwaist.HipWaistFragment
import org.southasia.ghru.ui.hipwaist.home.HipWaistHomeFragment
import org.southasia.ghru.ui.stationcheck.StationCheckDialogFragment

@Suppress("unused")
@Module
abstract class HipWaistBuilderModule {

    @ContributesAndroidInjector
    abstract fun HeightWeightHomeFragment(): HipWaistHomeFragment

    @ContributesAndroidInjector
    abstract fun ScanBarcodeFragment(): ScanBarcodeFragment

    @ContributesAndroidInjector
    abstract fun ManualEntryFragment(): ManualEntryHipWaistFragment
//
    @ContributesAndroidInjector
    abstract fun HeightFragment(): HipWaistFragment
//
    @ContributesAndroidInjector
    abstract fun HeightWeightQuestionaryFragment(): HipWaistQuestionnaireFragment
//
    @ContributesAndroidInjector
    abstract fun HeightWeightQuestionarySkipFragment(): HipWaistSkipFragment
//
    @ContributesAndroidInjector
    abstract fun ReasonDialogFragment(): ReasonDialogFragment
//
    @ContributesAndroidInjector
    abstract fun StationCheckDialogFragment(): StationCheckDialogFragment
//
    @ContributesAndroidInjector
    abstract fun CompletedDialogFragment(): CompletedDialogFragment
//
    @ContributesAndroidInjector
    abstract fun ErrorDialogFragment(): ErrorDialogFragment
//
//    @ContributesAndroidInjector
//    abstract fun WeightFragment(): WeightFragment
}