package org.southasia.ghru.ui.bodymeasurements.bp.questionnaire

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class BPQuestionnaireViewModel : ViewModel() {

    val haveArteriovenous = MutableLiveData<Boolean>()
    val hadSurgery = MutableLiveData<Boolean>()
    val lymphRemoved = MutableLiveData<Boolean>()
    val haveTrauma = MutableLiveData<Boolean>()

    fun setHaveArteriovenous(item: Boolean) {
        haveArteriovenous.value = item
    }

    fun setHadSurgery(item: Boolean) {
        hadSurgery.value = item
    }

    fun setLymphRemoved(item: Boolean) {
        lymphRemoved.value = item
    }

    fun setHaveTrauma(item: Boolean) {
        haveTrauma.value = item
    }

}
